package com.zuitt.wdc044;
//run- ./mvnw spring-boot:run
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication //annotation - shortcuts that make long, repetitive code shorter.
@RestController //tells spring boot that this will use endpoints for handling request and responses
public class Wdc044Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue =  "World")String name){
		return String.format("Hello %s", name);
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value = "user", defaultValue = "World")String user) {
		String s = "Hi ";
		return s.concat(user);
	}

}
